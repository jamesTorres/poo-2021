package com.principal.Ejercicios.parcial.pregunta1;


import java.util.ArrayList;
import java.util.List;

public class ProcesadorDatos {
    private List<Libro> libros;
    private Capitulo capitulo;

    public ProcesadorDatos() {
        this.libros = new ArrayList<>();
        this.capitulo = capitulo;
    }
    public void agregarLibro(Libro libro){
        libros.add(libro);
    }

    public void buscarLibro(String titulo, String nombreAutor, String ... datos){
        for(Libro libro : libros){
            if(libro.getTitulo().contains(titulo)){
                System.out.println("INFORMACION DEL LIBRO");
                System.out.println("Descripcion : "+libro.getDescripcion()+
                        "\nEditorial : "+libro.getEditorial()+
                        "\nCodigo : "+libro.getCodigo()+
                        "\nAutor : "+libro.getAutor()+
                        "\nAnio de Publicación : "+libro.getAnioPublicacion()+
                        "\nResumen : "+libro.getResumen());
            }
            else{
                System.out.println("No se encuentra el libro");
            }
        }
    }

}
