package com.principal.Ejercicios.parcial.pregunta1;

public class Aplicacion {
    public static void main(String[] args) {
        ProcesadorDatos procesador = new ProcesadorDatos();
        Libro tipo1 = new Libro();
        tipo1.setTitulo("Pinocho");
        tipo1.setAutor("Carlo");
        tipo1.setDescripcion("Un niño de madera aprende una reflexión acerca de la vida");
        tipo1.setEditorial("NORMA");
        tipo1.setAnioPublicacion("1883");
        tipo1.setCodigo("E306");
        tipo1.setResumen("La vida de un niño entra en conflicto por envidia de otras personas atravesando muchas"+
                " dificultades para entender lo valioso que es la vida");

        procesador.agregarLibro(tipo1);


        procesador.buscarLibro("Pinocho","Carlo","E306");

    }
}
