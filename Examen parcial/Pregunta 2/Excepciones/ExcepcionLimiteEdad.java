package com.principal.Ejercicios.parcial.pregunta2.Excepciones;

public class ExcepcionLimiteEdad extends Exception{
    public ExcepcionLimiteEdad(String message) {
        super(message);
    }
}
