package com.principal.Ejercicios.parcial.pregunta2.Enumeraciones;

public enum NivelEducativo {
    INICIAL("Inicial"),SECUNDARIA("Secundaria"),SUPERIOR("Superior");

    private String instruccion;

    NivelEducativo(String instruccion) {
        this.instruccion = instruccion;
    }
}
