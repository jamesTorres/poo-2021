package com.principal.Ejercicios.parcial.pregunta2.Interface;

import com.principal.Ejercicios.parcial.pregunta2.Excepciones.ExcepcionLimiteEdad;

public interface Evaluable {
    public void verificarEducacion() throws ExcepcionLimiteEdad;
}
