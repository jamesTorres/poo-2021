package com.principal.Ejercicios.parcial.pregunta2.Controlador;

import com.principal.Ejercicios.parcial.pregunta2.Enumeraciones.NivelEducativo;
import com.principal.Ejercicios.parcial.pregunta2.Clases.EvaluacionTrabajo;
import com.principal.Ejercicios.parcial.pregunta2.Clases.Postulante;
import com.principal.Ejercicios.parcial.pregunta2.Excepciones.ExcepcionLimiteEdad;

public class Aplicacion {
    public static void main(String[] args) {
        Postulante postulante = new Postulante();
        postulante.setNombre("Josue");
        postulante.setApellido("Hernandez");
        postulante.setEdad(19);
        postulante.setDireccion("Comas");
        postulante.setInstruccion(NivelEducativo.SUPERIOR);
        postulante.setTelefono("997519538");
        postulante.setDni("76841915");

        EvaluacionTrabajo evaluador = new EvaluacionTrabajo();
        try{
            evaluador.setPostulante(postulante);
            evaluador.verificarEducacion();
        }catch(ExcepcionLimiteEdad exepcion){
            exepcion.printStackTrace();
        }
        finally{
            System.out.println("Procesando ...");
        }





    }
}
