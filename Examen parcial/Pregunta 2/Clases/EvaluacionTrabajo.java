package com.principal.Ejercicios.parcial.pregunta2.Clases;

import com.principal.Ejercicios.parcial.pregunta2.Enumeraciones.NivelEducativo;
import com.principal.Ejercicios.parcial.pregunta2.Excepciones.ExcepcionLimiteEdad;
import com.principal.Ejercicios.parcial.pregunta2.Interface.Evaluable;

public class EvaluacionTrabajo implements Evaluable {
    private Postulante postulante;
    private Integer notaEvaluacion;

    public void verificarEducacion() throws ExcepcionLimiteEdad {
        if(postulante.getEdad()<18){
            throw new ExcepcionLimiteEdad("EDAD NO PERMITIDA");
        }
    }

    public void evaluarNota(){
        System.out.println("Evaluando...");
    }

    public Integer getNotaEvaluacion() {
        return notaEvaluacion;
    }

    public void setNotaEvaluacion(Integer notaEvaluacion) {
        this.notaEvaluacion = notaEvaluacion;
    }

    public Postulante getPostulante() {
        return postulante;
    }

    public void setPostulante(Postulante postulante) {
        this.postulante = postulante;
    }
}
