package com.principal.Ejercicios.parcial.pregunta2.Clases;

import com.principal.Ejercicios.parcial.pregunta2.Enumeraciones.NivelEducativo;

public class Postulante {
    private String dni;
    private String nombre;
    private String apellido;
    private NivelEducativo instruccion;
    private String direccion;
    private String telefono;
    private Integer edad;

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public NivelEducativo getInstruccion() {
        return instruccion;
    }

    public void setInstruccion(NivelEducativo instruccion) {
        this.instruccion = instruccion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }
}
