package com.principal.Ejercicios.parcial.pregunta3.Controlador;

import com.principal.Ejercicios.parcial.pregunta3.Enumeraciones.Material;
import com.principal.Ejercicios.parcial.pregunta3.Clases.Registro;
import com.principal.Ejercicios.parcial.pregunta3.Clases.Reloj;
import com.principal.Ejercicios.parcial.pregunta3.Clases.Ventas;
import com.principal.Ejercicios.parcial.pregunta3.Clases.Proceso;

public class Aplicacion {
    public static void main(String[] args) {
        Reloj tipo1 = new Reloj("Rolex", Material.ORO,350.70);
        Reloj tipo2 = new Reloj("Cartier",Material.PLATA,240.60);
        Reloj tipo3 = new Reloj("Montblanc",Material.COBRE,140.50);

        Ventas[] ventas = new Ventas[3];
        ventas[0] = new Ventas(tipo1,5);
        ventas[0].setPrecioTotal(ventas[0].calcularPrecioTotal());

        ventas[1] = new Ventas(tipo2,7);
        ventas[1].setPrecioTotal(ventas[1].calcularPrecioTotal());

        ventas[2] = new Ventas(tipo3,8);
        ventas[2].setPrecioTotal(ventas[2].calcularPrecioTotal());

        Registro registroVentas = new Registro();
        registroVentas.agregarVentas(ventas[0],ventas[1],ventas[2]);

        Proceso proceso = new Proceso();

        proceso.setRegistro(registroVentas);

        proceso.mostrarDatos();







    }
}
