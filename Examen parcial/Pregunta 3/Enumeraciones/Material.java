package com.principal.Ejercicios.parcial.pregunta3.Enumeraciones;

public enum Material {
    COBRE("Cobre"),PLATA("Plata"),ORO("Oro");
    private String tipo;

    Material(String tipo) {
        this.tipo = tipo;
    }
}
