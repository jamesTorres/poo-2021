package com.principal.Ejercicios.parcial.pregunta3.Clases;


import java.util.ArrayList;
import java.util.List;

public class Registro {
    List<Ventas> ventas;

    public Registro() {
        this.ventas = new ArrayList<>();
    }

    public void agregarVentas(Ventas ... pedidos){
        for(Ventas venta : pedidos){
            ventas.add(venta);
        }
    }

    public List<Ventas> getVentas() {
        return ventas;
    }

    public void setVentas(List<Ventas> ventas) {
        this.ventas = ventas;
    }
}
