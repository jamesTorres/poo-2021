package com.principal.Ejercicios.parcial.pregunta3.Clases;


import com.principal.Ejercicios.parcial.pregunta3.Clases.Reloj;

public class Ventas {
    private Reloj reloj;
    private Integer cantidad;
    private Double precioTotal;

    public Ventas(Reloj reloj, Integer cantidad) {
        this.reloj = reloj;
        this.cantidad = cantidad;
    }

    public Double calcularPrecioTotal(){
        return reloj.getPrecioUnitario() * cantidad;
    }

    public Reloj getReloj() {
        return reloj;
    }

    public void setReloj(Reloj reloj) {
        this.reloj = reloj;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Double getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(Double precioTotal) {
        this.precioTotal = precioTotal;
    }
}
