package com.principal.Ejercicios.parcial.pregunta3.Clases;

import com.principal.Ejercicios.parcial.pregunta3.Enumeraciones.Material;

public class Reloj {
    private String nombre;
    private Material material;
    private Double precioUnitario;

    public Reloj(String nombre, Material material, Double precioUnitario) {
        this.nombre = nombre;
        this.material = material;
        this.precioUnitario = precioUnitario;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(Double precioUnitario) {
        this.precioUnitario = precioUnitario;
    }
}
