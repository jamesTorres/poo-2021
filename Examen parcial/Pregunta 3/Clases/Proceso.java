package com.principal.Ejercicios.parcial.pregunta3.Clases;

import com.principal.Ejercicios.parcial.pregunta3.Clases.Registro;
import com.principal.Ejercicios.parcial.pregunta3.Clases.Ventas;

public class Proceso {
    private Registro registro;

    public void mostrarDatos(){
        for(Ventas venta : registro.getVentas()){
            System.out.println("Reloj : "+venta.getReloj().getNombre()+
                    "\nCantidad : "+venta.getCantidad());
        }
    }

    public Registro getRegistro() {
        return registro;
    }

    public void setRegistro(Registro registro) {
        this.registro = registro;
    }
}
