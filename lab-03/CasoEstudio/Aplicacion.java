package com.principal.Ejercicios.ejercicio5;

public class Aplicacion {
    public static void main(String[] args) {
        Registro[] registro = new Registro[3];
        Pedido Tipo1 = new Pedido("Frambuesas",48.42,"Urubamba","Cuzco");
        Pedido Tipo2 = new Pedido("Arándanos",100.12,"Urubamba","Cuzco");
        PedidoExtra Tipo3 = new PedidoExtra("frambuesas",47.53,"Urubamba","Rio Branco-Brasil",
                "Maíz",50);

        Cliente clienteA = new Cliente("James","76841911");

        registro[0] = new Registro();
        registro[0].setPedido(Tipo1);
        registro[0].setCliente(clienteA);
        registro[0].setPrecioUnitario(142);
        registro[0].setMediodeEnvio("Terrestre");

        Cliente clienteB = new Cliente("Pablo","74152032");

        registro[1] = new Registro();
        registro[1].setPedido(Tipo2);
        registro[1].setCliente(clienteB);
        registro[1].setPrecioUnitario(178);
        registro[1].setMediodeEnvio("Terrestre");

        Cliente clienteC = new Cliente("Josue","75182433");

        registro[2] = new Registro();
        registro[2].setPedido(Tipo3);
        registro[2].setCliente(clienteC);
        registro[2].setPrecioUnitario(200);
        registro[2].setMediodeEnvio("Fluvial");


    }
}
