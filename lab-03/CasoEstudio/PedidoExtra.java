package com.principal.Ejercicios.ejercicio5;

public class PedidoExtra extends Pedido{
    private String vegetales;
    private double cantidadkilogramosVegetales;

    public PedidoExtra(String fruta, double cantidadKilogramos, String lugardeSalida, String lugardeEntrega, String vegetales, double cantidadKilogramosVegetales) {
        super(fruta, cantidadKilogramos, lugardeSalida, lugardeEntrega);
        this.vegetales = vegetales;
        this.cantidadkilogramosVegetales = cantidadKilogramosVegetales;
    }

    public String getVegetales() {
        return vegetales;
    }

    public void setVegetales(String vegetales) {
        this.vegetales = vegetales;
    }

    public double getCantidadkilogramosVegetales() {
        return cantidadkilogramosVegetales;
    }

    public void setCantidadkilogramosVegetales(double cantidadkilogramosVegetales) {
        this.cantidadkilogramosVegetales = cantidadkilogramosVegetales;
    }
}

