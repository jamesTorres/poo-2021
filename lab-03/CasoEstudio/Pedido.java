package com.principal.Ejercicios.ejercicio5;

public class Pedido {
    private String fruta;
    private double cantidadKilogramosFrutas;
    private String lugardeSalida;
    private String lugardeEntrega;

    public Pedido(String fruta, double cantidadKilogramosFrutas,String lugardeSalida, String lugardeEntrega) {
        this.fruta = fruta;
        this.cantidadKilogramosFrutas = cantidadKilogramosFrutas;
        this.lugardeSalida = lugardeSalida;
        this.lugardeEntrega = lugardeEntrega;
    }

    public double getCantidadKilogramosFrutas() {
        return cantidadKilogramosFrutas;
    }

    public void setCantidadKilogramos(double cantidadKilogramosFrutas) {
        this.cantidadKilogramosFrutas = cantidadKilogramosFrutas;
    }

    public String getFruta() {
        return fruta;
    }

    public void setFruta(String fruta) {
        this.fruta = fruta;
    }

    public String getLugardeSalida() {
        return lugardeSalida;
    }

    public void setLugardeSalida(String lugardeSalida) {
        this.lugardeSalida = lugardeSalida;
    }

    public String getLugardeEntrega() {
        return lugardeEntrega;
    }

    public void setLugardeEntrega(String lugardeEntrega) {
        this.lugardeEntrega = lugardeEntrega;
    }
}
