package com.principal.Ejercicios.ejercicio5;

public class Registro {
    private Pedido pedido;
    private Cliente cliente;
    private int precioUnitario;
    private String mediodeEnvio;

    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public int getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(int precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    public String getMediodeEnvio() {
        return mediodeEnvio;
    }

    public void setMediodeEnvio(String mediodeEnvio) {
        this.mediodeEnvio = mediodeEnvio;
    }
}
