package com.principal.Ejercicios.ejercicio6;

public class Estudiante {
    private String nombre;
    private String apellido;
    private int notaCurso;

    public Estudiante(String nombre, String apellido, int notaCurso) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.notaCurso = notaCurso;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getNotaCurso() {
        return notaCurso;
    }

    public void setNotaCurso(int notaCurso) {
        this.notaCurso = notaCurso;
    }
}
