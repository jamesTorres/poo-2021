package com.principal.Ejercicios.ejercicio6;

public class Curso{
    private int creditos;
    private String codigo;
    private int horasTotales;

    public Curso(int creditos, String codigo, int horasTotales) {
        this.creditos = creditos;
        this.codigo = codigo;
        this.horasTotales = horasTotales;
    }

    public int getCreditos() {
        return creditos;
    }

    public void setCreditos(int creditos) {
        this.creditos = creditos;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public int getHorasTotales() {
        return horasTotales;
    }

    public void setHorasTotales(int horasTotales) {
        this.horasTotales = horasTotales;
    }
}
