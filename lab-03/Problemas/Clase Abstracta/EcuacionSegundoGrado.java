package com.principal.Ejercicios.ejercicio6;

import java.util.Scanner;

import static java.lang.Math.pow;
import static java.lang.StrictMath.sqrt;

public class EcuacionSegundoGrado extends Operaciones{
    @Override
    public Double[] buscadorRaiz(double[] coeficientes) {
        double raiz = 0;
        Double[] raices = new Double[2];
        raiz = -coeficientes[1] - sqrt(pow(coeficientes[1],2) - 4*coeficientes[2]*coeficientes[0]);
        raiz = raiz / (coeficientes[0]*2);
        raices[0] = raiz;
        raiz = -coeficientes[1] + sqrt(pow(coeficientes[1],2) - 4*coeficientes[2]*coeficientes[0]);
        raiz = raiz / (coeficientes[0]*2);
        raices[1] = raiz;
        return raices;
    }

    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        double[] coeficientes = new double[3];
        for (int i = 0; i < 3; i++){
            System.out.print("Coeficiente "+(i+1)+":");
            coeficientes[i] = entrada.nextDouble();
        }
        Double[] raices = new Double[2];
        EcuacionSegundoGrado variable = new EcuacionSegundoGrado();
        raices = variable.buscadorRaiz(coeficientes);
        System.out.println("Raices:");
        for(double x : raices){
            System.out.println(x);
        }
    }
}
