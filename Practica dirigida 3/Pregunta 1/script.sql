create table departamento(
                             codigo_departamento varchar(10),
                             nombre varchar(100),
                             ubicacion varchar(50)
);

create table empleado(
                         codigo_empleado varchar(20),
                         codigo_departamento varchar(10),
                         nombres varchar(50),
                         apellidos varchar(100)
);

create table asignacion(
                           id_asignacion numeric(5),
                           codigo_empleado varchar(20),
                           id_actividad numeric(4),
                           presupuesto numeric(9,2)
);

create table actividad(
                          id_actividad numeric(4),
                          nombre varchar(50),
                          prioridad numeric(1)
);

alter table departamento add primary key(codigo_departamento);
alter table empleado add foreign key(codigo_departamento)
    references departamento(codigo_departamento);
alter table empleado add primary key(codigo_empleado);
alter table asignacion add foreign key(codigo_empleado)
    references empleado(codigo_empleado);
alter table asignacion add primary key(id_asignacion);
alter table actividad add primary key(id_actividad);
alter table asignacion add foreign key(id_actividad)
    references actividad(id_actividad);

insert into departamento(codigo_departamento, nombre, ubicacion)
values ('23A','Lima','Carabayllo');
insert into departamento(codigo_departamento, nombre, ubicacion)
values ('40B','Arequipa','Chivay');

insert into empleado(codigo_empleado, codigo_departamento, nombres, apellidos)
values ('A1','23A','James Alberto','Torres Lucas');
insert into empleado(codigo_empleado, codigo_departamento, nombres, apellidos)
values ('A2','23A','Jesus Dario','Paredes Rodriguez');
insert into empleado(codigo_empleado, codigo_departamento, nombres, apellidos)
values ('B1','40B','Paolo Mario','Duran Alcides');

insert into actividad(id_actividad, nombre, prioridad)
values (1, 'Imprimir',1);
insert into asignacion(id_asignacion, codigo_empleado, id_actividad, presupuesto)
values (1,'A1',1,14.50);
-- Obtener los nombres, apellidos y el nombre del departamento de todos los empleados.
select e.nombres, e.apellidos, d.nombre
from empleado e
         join departamento d on ( d.codigo_departamento = e.codigo_departamento);

-- Registrar una nueva actividad.
insert into actividad(id_actividad, nombre, prioridad)
values (2, 'Comprar planos',2);
-- Obtener las asignaciones de un determinado empleado.
select e.nombres, ac.nombre actividad, a.presupuesto
from empleado e
         join asignacion a on a.codigo_empleado=e.codigo_empleado
         join actividad ac on ac.id_actividad=a.id_actividad
where e.codigo_empleado='A1';

-- Actualizar la ubicación de un determinado departamento