create table usuario(
    id_usuario numeric(9),
    nombres varchar(100),
    apellidos varchar(100),
    correo varchar(200),
    administrador char(1),
    clave varchar(200)
);

alter table usuario add primary key (id_usuario);
create sequence secuencia_usuario start with 1;

insert into usuario(id_usuario, nombres, apellidos, correo, administrador, clave)
values (nextval(secuencia_usuario), 'James Alberto','Torres Lucas','james.torres.l@uni.pe','1','12345');
insert into usuario(id_usuario, nombres, apellidos, correo, administrador, clave)
values (nextval(secuencia_usuario), 'Luis','Marquez','luis.l@uni.pe','0','2045');
insert into usuario(id_usuario, nombres, apellidos, correo, administrador, clave)
values (nextval(secuencia_usuario), 'Jose','Tormez','jose.t@uni.pe','0','jose');
insert into usuario(id_usuario, nombres, apellidos, correo, administrador, clave)
values (nextval(secuencia_usuario), 'Manuel','Castilla','manuel.c@uni.pe','0','manuelcas');
insert into usuario(id_usuario, nombres, apellidos, correo, administrador, clave)
values (nextval(secuencia_usuario), 'Tomas','Torca','tomas.t@uni.pe','0','torca123');

-- Busqueda de usuarios
select u.id_usuario,u.nombres, u.apellidos, u.correo, u.administrador,u.clave
from usuario u
where upper(u.nombres) like '%%' OR upper(u.apellidos) like '%%' OR upper(u.correo) like '%%';

-- Validar login d
select u.correo, u.clave
from usuario u
where u.correo = 'james.torres.l@uni.pe' and u.clave = '12345';

-- Actualizando datos
update usuario
set clave = '147'
where id_usuario = 4;


select * from usuario;



