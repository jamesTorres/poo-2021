package com.example.aplicacion.dao;

import com.example.aplicacion.dto.Usuario;

import java.util.List;

public interface Dao {
    public Usuario obtenerLogin(Usuario usuario);
    public List<Usuario> obtenerUsuario(Usuario usuario);
    public Usuario actualizarUsuario(Usuario usuario);
}
