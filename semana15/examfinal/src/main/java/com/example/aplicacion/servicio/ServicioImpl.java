package com.example.aplicacion.servicio;

import com.example.aplicacion.dao.Dao;
import com.example.aplicacion.dto.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ServicioImpl implements Servicio {
    @Autowired
    private Dao dao;

    public List<Usuario> obtenerUsuario(Usuario usuario) {
        return dao.obtenerUsuario(usuario);
    }

    public Usuario obtenerLogin(Usuario usuario) {
        return dao.obtenerLogin(usuario);
    }

    public Usuario actualizarUsuario(Usuario usuario) {
        return dao.actualizarUsuario(usuario);
    }
}
