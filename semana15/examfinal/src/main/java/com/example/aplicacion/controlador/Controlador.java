package com.example.aplicacion.controlador;

import com.example.aplicacion.dto.Usuario;
import com.example.aplicacion.dto.res.RespuestaUsuario;
import com.example.aplicacion.servicio.Servicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = {"*"})
public class Controlador {
    @Autowired
    private Servicio servicio;

    @RequestMapping(
            value = "obtener-usuarios",
            method = RequestMethod.POST,
            consumes = "application/json;charset=utf-8",
            produces = "application/json;charset=utf-8"
    )
    public @ResponseBody RespuestaUsuario obtenerUsuario(@RequestBody Usuario usuario){
        RespuestaUsuario respuestaUsuario = new RespuestaUsuario();
        respuestaUsuario.setLista(servicio.obtenerUsuario(usuario));
        return respuestaUsuario;
    }

    @RequestMapping(
            value = "obtener-login",
            method = RequestMethod.POST,
            consumes = "application/json;charset=utf-8",
            produces = "application/json;charset=utf-8"
    )
    public @ResponseBody Usuario obtenerLogin(@RequestBody Usuario usuario){
        return servicio.obtenerLogin(usuario);
    }

    @RequestMapping(
            value = "actualizar-usuario",
            method = RequestMethod.POST,
            consumes = "application/json;charset=utf-8",
            produces = "application/json;charset=utf-8"
    )
    public @ResponseBody Usuario actualizarUsuario(@RequestBody Usuario usuario){
        return servicio.actualizarUsuario(usuario);
    }

}
