package com.example.aplicacion.dto.res;

import com.example.aplicacion.dto.Usuario;
import lombok.Data;

import java.util.List;

@Data
public class RespuestaUsuario {
    private List<Usuario> lista;
}
