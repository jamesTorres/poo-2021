package com.example.aplicacion.dao;

import com.example.aplicacion.dto.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class DaoImpl implements Dao{
    @Autowired
    private JdbcTemplate jdbcTemplate;
    private Connection conexion;

    private void obtenerConexion(){
        try {
            this.conexion = jdbcTemplate.getDataSource().getConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private void cerrarConexion(ResultSet resultado, Statement sentencia){
        try {
            if(resultado != null) resultado.close();
            if(sentencia != null) sentencia.close();
            this.conexion.commit();
            this.conexion.close();
            this.conexion = null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public List<Usuario> obtenerUsuario(Usuario usuario){
        List<Usuario> usuarios = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        sb.append("select u.id_usuario,u.nombres, u.apellidos, u.correo, u.administrador,u.clave\n" +
                "from usuario u\n" +
                "where upper(u.nombres) like ? or upper(u.apellidos) like ? or upper(u.correo) like ? ");
        obtenerConexion();
        // funciona cuando pones los tres datos en postman, en el sql funciona cuando solo pongo uno o ninguno
        try {
            PreparedStatement sentencia = this.conexion.prepareStatement(sb.toString());
            sentencia.setString(1, "%"+usuario.getNombres().toUpperCase()+"%");
            sentencia.setString(2, "%"+usuario.getApellidos().toUpperCase()+"%");
            sentencia.setString(3, "%"+usuario.getCorreo().toUpperCase()+"%");
            ResultSet resultado = sentencia.executeQuery();
            while(resultado.next()) {
                Usuario u = extraerUsuario(resultado);
                usuarios.add(u);
            }
            cerrarConexion(resultado,sentencia);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return usuarios;
    }

    public Usuario obtenerLogin(Usuario usuario) {
        StringBuilder sb = new StringBuilder();
        sb.append("select u.correo, u.clave\n" +
                "from usuario u\n" +
                "where u.correo = ? and u.clave = ? ");
        try {
            obtenerConexion();
            PreparedStatement sentencia = conexion.prepareStatement(sb.toString());
            sentencia.setString(1,usuario.getCorreo());
            sentencia.setString(2, usuario.getClave());
            ResultSet resultado = sentencia.executeQuery();
            while(resultado.next()) {
                Usuario u = extraerUsuario(resultado);
            }
            cerrarConexion(resultado,sentencia);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return usuario;
    }


    public Usuario actualizarUsuario(Usuario usuario) {
        StringBuilder sb = new StringBuilder();
        sb.append("update usuario\n" +
                "set clave = ?\n" +
                "where id_usuario = ?");
        obtenerConexion();
        try {
            PreparedStatement sentencia = this.conexion.prepareStatement(sb.toString());
            sentencia.setString(1, usuario.getClave());
            sentencia.setInt(2, usuario.getId_usuario());
            sentencia.executeQuery();
            cerrarConexion(null, sentencia);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return usuario;
    }

    public Usuario extraerUsuario(ResultSet resultado) throws SQLException{
        Usuario usuario = new Usuario(resultado.getInt("id_usuario"),
                resultado.getString("nombres"),
                resultado.getString("apellidos"),
                resultado.getString("correo"),
                "1".equals(resultado.getString("administrador")) ? true : false,
                resultado.getString("clave")
        );
        return usuario;
    }



}
