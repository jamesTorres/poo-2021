package com.example.aplicacion.servicio;

import com.example.aplicacion.dto.Usuario;

import java.util.List;

public interface Servicio {
    public Usuario obtenerLogin(Usuario usuario);
    public List<Usuario> obtenerUsuario(Usuario usuario);
    public Usuario actualizarUsuario(Usuario usuario);
}
