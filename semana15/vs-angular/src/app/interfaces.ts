export interface Usuario{
    id_usuario? : number,
    nombres? : string,
    apellidos? : string,
    correo? : string,
    administrador? : boolean,
    clave? : string,
}

export interface sesionUsuario{
    correo : string, 
    clave : string,
}

export interface RespuestaUsuario{
    lista : Usuario[];
}

