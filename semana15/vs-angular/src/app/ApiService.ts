import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { RespuestaUsuario, Usuario } from "./interfaces";
import { retry, catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ApiService {
    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json;charset=utf-8' 
        })  
    };
    errorHandl(error : any) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            errorMessage = error.error.message;    
        } 
        else {
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        console.log(errorMessage);
        return throwError(errorMessage);
    }
    constructor(private http: HttpClient) { }

    obtenerUsuarios(data:Usuario): Observable<RespuestaUsuario>{
        return this.http.post<RespuestaUsuario>('http://localhost:8080/obtener-usuarios', data, this.httpOptions)
        .pipe(
            retry(1),
            catchError(this.errorHandl)
        );
    }

    obtenerLogin(data : Usuario) : Observable<Usuario>{
        return this.http.post<Usuario>('http://localhost:8080/obtener-login', data, this.httpOptions)
        .pipe(
            retry(1),
            catchError(this.errorHandl)
        );
    }

    actualizarUsuario(data : Usuario) : Observable<Usuario>{
        return this.http.post<Usuario>('http://localhost:8080/actualizar-usuario', data, this.httpOptions)
        .pipe(
            retry(1),
            catchError(this.errorHandl)
        );
    }

}