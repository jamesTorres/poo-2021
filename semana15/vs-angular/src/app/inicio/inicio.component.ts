import { Component, OnInit } from '@angular/core';
import { ApiService } from '../ApiService';
import { Usuario } from '../interfaces';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.scss']
})
export class InicioComponent implements OnInit {
  nombres:string="";
  apellidos:string="";
  correo:string="";
  lista: Usuario[] = [];


  constructor(private api:ApiService) { }

  ngOnInit(): void {
  }

  buscarUsuario(): void {
    console.log(this.nombres);
    console.log(this.apellidos);
    console.log(this.correo);
    const usuario : Usuario = {
      id_usuario : undefined,
      nombres : this.nombres,
      apellidos : this.apellidos,
      correo : this.correo,
      administrador : undefined,
      clave : undefined

    }
    this.api.obtenerUsuarios(usuario).subscribe(respuesta =>{
      this.lista = respuesta.lista;
    });
  }

}
