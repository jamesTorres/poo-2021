package com.principal.Ejercicios.ejercicio6;

public class Mamifero extends Animal{
    private String color;

    public Mamifero(String tamaño, String color) {
        super(tamaño);
        this.color = color;
    }

    public void alimentarse() {
        System.out.println("El mamifero es de tamaño "+getTamaño()+" y es de color "+getColor()+".");
    }

    public void correr() {
        System.out.println("El mamifero de color "+getColor()+" y esta corriendo.");
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
