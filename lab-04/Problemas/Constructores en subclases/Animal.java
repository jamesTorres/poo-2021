package com.principal.Ejercicios.ejercicio6;

public class Animal {
    private String tamaño;

    public Animal(String tamaño) {
        this.tamaño = tamaño;
    }

    public String getTamaño() {
        return tamaño;
    }

    public void setTamaño(String tamaño) {
        this.tamaño = tamaño;
    }

    public void alimentarse(){
        System.out.println("El animal se ha alimentado");
    };
    public void correr(){
        System.out.println("El animal a empezado a correr");
    };
}
