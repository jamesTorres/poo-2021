package com.principal.Ejercicios.ejercicio6;

import javax.swing.*;

public class Aplicacion{
    public static void main(String[] args) {
        int terminos;
        terminos = Integer.parseInt(JOptionPane.showInputDialog("N° de terminos : "));
        System.out.println("La suma de la serie Fibonacci es : "+Fibonacci.sumarSerie(terminos));
        System.out.println("La suma de la serie Fibonacci Intercambiado es : "+FibonacciIntercambiado.sumarSerie(terminos));

    }
}
