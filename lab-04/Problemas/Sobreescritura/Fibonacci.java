package com.principal.Ejercicios.ejercicio6;

public class Fibonacci {
    public static int terminoN(int terminos){
        if(terminos == 0){
            return 0;
        }
        else if(terminos == 1){
            return 1;
        }
        else{
            return terminoN(terminos-1) + terminoN(terminos-2);
        }
    }
    public static int sumarSerie(int terminos){
        int suma = 0;
        for(int i=0; i < terminos ; i++){
            suma += terminoN(i);
        }
        return suma;
    }


}
