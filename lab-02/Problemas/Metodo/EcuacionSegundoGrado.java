package Problemas;

import java.util.Scanner;

public class EcuacionSegundoGrado {
    private int coeficiente1;
    private int coeficiente2;
    private int coeficiente3;
    private int determinante;
    private double primeraRaiz;
    private double segundaRaiz;
    
    public static void main(String[] args){
        Scanner Entrada = new Scanner(System.in);
        EcuacionSegundoGrado variable = new EcuacionSegundoGrado();
        System.out.println("Indique los coefiecientes en orden: ");
        variable.setCoeficiente1(Entrada.nextInt());
        variable.setCoeficiente2(Entrada.nextInt());
        variable.setCoeficiente3(Entrada.nextInt());
        variable.determinante();
        variable.calcularRaices();
    }
    
    public void calcularRaices(){
        if(determinante >= 0){
            primeraRaiz = (coeficiente2 - Math.sqrt(determinante))/(2*coeficiente1);
            segundaRaiz = (coeficiente2 + Math.sqrt(determinante))/(2*coeficiente1);
            System.out.println("La primera raiz: "+primeraRaiz);
            System.out.println("La segunda raiz: "+segundaRaiz);
        }
        else{
            System.out.println("Hay raices imaginarias");
        }
    }
    
    public void determinante(){
        determinante = (int)Math.pow(coeficiente2,2) - 4*coeficiente1*coeficiente3;
    }

    public double getPrimeraRaiz() {
        return primeraRaiz;
    }

    public void setPrimeraRaiz(double primeraRaiz) {
        this.primeraRaiz = primeraRaiz;
    }

    public double getSegundaRaiz() {
        return segundaRaiz;
    }

    public void setSegundaRaiz(double segundaRaiz) {
        this.segundaRaiz = segundaRaiz;
    }

    public int getCoeficiente1() {
        return coeficiente1;
    }

    public void setCoeficiente1(int coeficiente1) {
        this.coeficiente1 = coeficiente1;
    }

    public int getCoeficiente2() {
        return coeficiente2;
    }

    public void setCoeficiente2(int coeficiente2) {
        this.coeficiente2 = coeficiente2;
    }

    public int getCoeficiente3() {
        return coeficiente3;
    }

    public void setCoeficiente3(int coeficiente3) {
        this.coeficiente3 = coeficiente3;
    }
}
