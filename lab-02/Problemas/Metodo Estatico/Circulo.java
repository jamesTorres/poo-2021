package Problemas;

import java.util.Scanner;

public class Circulo {
    private static double radio;

    public static double calcularArea(double radio){
        double resultado;
        resultado = Math.PI * Math.pow(radio,2);
        return resultado;
    }
    
    public static void main(String[] args){
        Scanner Entrada = new Scanner(System.in);
        System.out.println("Radio: ");
        Circulo.radio = Entrada.nextDouble();
        System.out.println("El area del circulo es: "+Circulo.calcularArea(Circulo.radio));   
    }
}
