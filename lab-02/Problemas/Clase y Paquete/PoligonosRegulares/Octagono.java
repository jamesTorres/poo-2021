package PoligonosRegulares;

import AreaPerimetro.Area;
import java.util.Scanner;

public class Octagono {
    public static void main(String[] args){
        System.out.println("OCTAGONO");
        Area octagono = new Area();
        Scanner Entrada = new Scanner(System.in);
        double longitud;
        System.out.print("Longitud: ");
        longitud = Entrada.nextDouble();
        octagono.perimetroOctogono(longitud);
        octagono.areaOctogono(longitud);
    }
}
