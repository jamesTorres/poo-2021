package PoligonosRegulares;

import AreaPerimetro.Area;
import java.util.Scanner;

public class Pentagono {
    public static void main(String[] args){
        System.out.println("PENTAGONO");
        Area pentagono = new Area();
        Scanner Entrada = new Scanner(System.in);
        double longitud;
        System.out.print("Longitud: ");
        longitud = Entrada.nextDouble();
        pentagono.perimetroPentagono(longitud);
        pentagono.areaPentagono(longitud);
    }
}
