package PoligonosRegulares;

import AreaPerimetro.Area;
import java.util.Scanner;

public class Hexagono {
    public static void main(String[] args){
        System.out.println("HEXAGONO");
        Area hexagono = new Area();
        Scanner Entrada = new Scanner(System.in);
        double longitud;
        System.out.print("Longitud: ");
        longitud = Entrada.nextDouble();
        hexagono.perimetroHexagono(longitud);
        hexagono.areaHexagono(longitud);
    }
}
