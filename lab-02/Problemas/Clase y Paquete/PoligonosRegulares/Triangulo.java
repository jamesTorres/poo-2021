package PoligonosRegulares;

import AreaPerimetro.Area;
import java.util.Scanner;

public class Triangulo {
    public static void main(String[] args){
        System.out.println("TRIANGULO");
        Area triangulo = new Area();
        Scanner Entrada = new Scanner(System.in);
        double longitud;
        System.out.print("Longitud: ");
        longitud = Entrada.nextDouble();
        triangulo.perimetroTriangulo(longitud);
        triangulo.areaTriangulo(longitud);
    }
}
