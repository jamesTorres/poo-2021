package PoligonosRegulares;

import AreaPerimetro.Area;
import java.util.Scanner;

public class Cuadrado {
    public static void main(String[] args){
        System.out.println("CUADRADO");
        Area cuadrado = new Area();
        Scanner Entrada = new Scanner(System.in);
        double longitud;
        System.out.print("Longitud: ");
        longitud = Entrada.nextDouble();
        cuadrado.perimetroCuadrado(longitud);
        cuadrado.areaCuadrado(longitud);
    }
}
