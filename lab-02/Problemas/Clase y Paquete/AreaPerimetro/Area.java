package AreaPerimetro;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class Area {
    private double resultado;
    //CUADRADO
    public void areaCuadrado(double longitud){
        resultado = pow(longitud,2);
        System.out.println("Area: "+resultado+" m^2");
    }
    public void perimetroCuadrado(double longitud){
        resultado = longitud * 4 ;
        System.out.println("Perimetro: "+resultado+" m");
    }
    //PENTAGONO
    public void areaPentagono(double longitud){
        resultado = (5/4)*pow(longitud,2)*((sqrt(5)+1)/sqrt(10-2*sqrt(5)));
        System.out.println("Area: "+resultado+" m^2");
    }
    public void perimetroPentagono(double longitud){
        resultado = longitud * 5;
        System.out.println("Permietro: "+resultado+" m");
    }
    //TRIANGULO
    public void areaTriangulo(double longitud){
        resultado = pow(longitud,2)*(sqrt(3)/4);
        System.out.println("Area: "+resultado+" m^2");
    }
    public void perimetroTriangulo(double longitud){
        resultado = longitud * 3;
        System.out.println("Perimetro: "+resultado+" m");
    }
    //HEXAGONO
    public void areaHexagono(double longitud){
        resultado = (3/2)*pow(longitud,2)*sqrt(3);
        System.out.println("Area: "+resultado+" m^2");
    }
    public void perimetroHexagono(double longitud){
        resultado = longitud * 6;
        System.out.println("Perimetro: "+longitud+" m");
    }
    //OCTOGONO
    public void areaOctogono(double longitud){
        resultado = 2*pow(longitud,2)*(sqrt(2)+1);
        System.out.println("Area: "+resultado+" m^2");
    }
    public void perimetroOctogono(double longitud){
        resultado = longitud * 8;
        System.out.println("Perimetro: "+resultado+" m");
    }
    
}
