package com.principal.Ejercicios.REPASO;

public class Operaciones {
    public static int convertirBaseNovena(int variable){
        int suma = 0;
        int auxiliar = variable;
        int i = 0;
        while(variable > 0){
            auxiliar = variable % 9;
            variable = variable / 9;
            suma = suma + auxiliar * (int)Math.pow(10,i);
            i++;
        }
        return suma;
    }

    public int convertirBaseOctava(int variable){
        int suma = 0;
        int auxiliar = variable;
        int i = 0;
        while(variable > 0){
            auxiliar = variable % 8;
            variable = variable / 8;
            suma = suma + auxiliar * (int)Math.pow(10,i);
            i++;
        }
        return suma;
    }

    public int convertirBaseDecimal(int variable,int base){
        int suma = 0;
        int auxiliar;
        int i = 0;
        while(variable>0){
            auxiliar = variable % 10;
            variable = variable / 10;
            suma = suma + auxiliar * (int)Math.pow(base,i);
            i++;
        }
        return suma;
    }

    public boolean verificador(int variable, int base){
        boolean verificador = false;
        int i = 1;
        int auxiliar = variable;
        while(variable > 0){
            variable = variable / 10;
            i++;
        }
        for(int j = 0; j<i; j++){
            if(auxiliar%10>=base){
                verificador = true;
            }
            auxiliar = auxiliar/10;
        }
        return verificador;
    }

    public int suma(int x, int y){
        int suma, resultadoFinal;
        suma = x + y;
        resultadoFinal = Operaciones.convertirBaseNovena(suma);
        return resultadoFinal;
    }

    public int resta(int x, int y){
        int resta, resultadoFinal;
        if(x-y>0){
            resta = x - y;
        }
        else{
            resta = y- x;
        }
        resultadoFinal = Operaciones.convertirBaseNovena(resta);
        return resultadoFinal;
    }
}
