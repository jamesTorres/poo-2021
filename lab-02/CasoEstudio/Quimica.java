package com.principal.Ejercicios.REPASO;

import java.util.Scanner;

public class Quimica {
    public static void main(String[] args) {
        Scanner Entrada = new Scanner(System.in);
        int temperaturaInicial,temperaturaConvertida,conversionDecimal;
        boolean verificador;
        Operaciones operador = new Operaciones();
        System.out.println("Numeros en base 8");
        do{
            System.out.print("Temperatura en Grados Celsius : ");
            temperaturaInicial = Entrada.nextInt();
            verificador = operador.verificador(temperaturaInicial,8);
        }while(!(verificador == false));
        System.out.println("CONVERSIÓN A GRADOS KELVIN");
        conversionDecimal = operador.convertirBaseDecimal(temperaturaInicial,8);
        System.out.println("Grado Kelvin : "+operador.convertirBaseOctava(conversionDecimal+273));
    }
}
