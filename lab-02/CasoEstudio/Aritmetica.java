package com.principal.Ejercicios.REPASO;

import java.util.Scanner;

public class Aritmetica {
    public static void main(String[] args){
        Scanner Entrada = new Scanner(System.in);
        int n1, n2, sistema1, sistema2, suma, resta;
        boolean verificador;
        Operaciones auxiliar = new Operaciones();
        //RESTRICCIONES
        do{
            System.out.print("Sistema de Numeracion: ");
            sistema1 = Entrada.nextInt();
        }while(!(sistema1>0));
        do{
            System.out.print("Numero: ");
            n1 = Entrada.nextInt();
            verificador = auxiliar.verificador(n1,sistema1);
        }while(!(verificador==false));
        do{
            System.out.print("Sistema de Numeracion: ");
            sistema2 = Entrada.nextInt();
        }while(!(sistema2>0));
        do{
            System.out.print("Numero: ");
            n2 = Entrada.nextInt();
            verificador = auxiliar.verificador(n2,sistema2);
        }while(!(verificador==false));
        //CAMBIO DE BASE
        n1 = auxiliar.convertirBaseDecimal(n1,sistema1);
        n2 = auxiliar.convertirBaseDecimal(n2,sistema2);
        System.out.println("La suma en base 9 es: "+auxiliar.suma(n1, n2));
        System.out.println("La resta en base 9 es: "+auxiliar.resta(n1,n2));
    }
}

