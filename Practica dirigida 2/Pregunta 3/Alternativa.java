package com.principal.Ejercicios.Ejerciciosx2.problema3;

import java.util.Map;

public class Alternativa {
    private String enunciado;
    private String respuesta;

    public Alternativa(String enunciado, String respuesta) {
        this.enunciado = enunciado;
        this.respuesta = respuesta;
    }

    public String getEnunciado() {
        return enunciado;
    }
}
