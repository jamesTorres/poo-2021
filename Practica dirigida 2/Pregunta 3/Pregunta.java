package com.principal.Ejercicios.Ejerciciosx2.problema3;

import java.util.ArrayList;
import java.util.List;

public class Pregunta implements Registro{
    private String enunciado;
    private List<Alternativa> alternativas;

    public Pregunta(String enunciado) {
        this.enunciado = enunciado;
        this.alternativas = new ArrayList<>();
    }

    public void agregarAlternativa(Alternativa ... alternativas){
        for(Alternativa item: alternativas){
            this.alternativas.add(item);
        }

    }


}
