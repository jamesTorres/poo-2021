package com.principal.Ejercicios.Ejerciciosx2.problema3;

public class Aplicacion {
    public static void main(String[] args) {
        Registro[] pregunta = new Registro[3];
        Evaluacion interrogantes = new Evaluacion();
        pregunta[0] = new Pregunta("¿Qué establecimiento desea conocer de la UNI?");

        Alternativa primera = new Alternativa("Museo","Av. Túpac Amaru 210, Cercado de Lima 15333");
        Alternativa segunda = new Alternativa("Biblioteca Central","Cercado de Lima 15333");
        Alternativa tercera = new Alternativa("Coliseo","Tupac Amaru, Cercado de Lima 15333");

        pregunta[0].agregarAlternativa(primera,segunda,tercera);
        interrogantes.agregarPreguntas(pregunta[0]);

        pregunta[1] = new Pregunta("¿Qué facultad desea visitar?");

        Alternativa cuarta = new Alternativa("FIIS","Av. Túpac Amaru 280, Cercado de Lima 15333");
        Alternativa quinta = new Alternativa("FIQM","Av. Túpac Amaru 210, Rímac 15333");
        Alternativa sexta = new Alternativa("FIA","Av. Túpac Amaru 280, Cercado de Lima 15333");

        pregunta[1].agregarAlternativa(cuarta,quinta,sexta);
        interrogantes.agregarPreguntas(pregunta[1]);

        pregunta[2] = new Pregunta("¿Hace cuántos años no visita la UNI?");

        Alternativa septima = new Alternativa("Un año.","Muy poco tiempo");
        Alternativa octava = new Alternativa("Tres años","Tiempo intermedio");
        Alternativa novena = new Alternativa("Más de tres años","Largo tiempo");

        pregunta[2].agregarAlternativa(septima,octava,novena);
        interrogantes.agregarPreguntas(pregunta[2]);



    }

}
