package com.principal.Ejercicios.Ejerciciosx2;

public class Aplicacion {
    public static void main(String[] args) {
        try{
            Personas[] asistentes = new Personas[2];
            asistentes[0]= new Personas("7841911","Maria Fernanda","Tinoco",
                    "Rufasto",41);
            asistentes[1]= new Personas("7418325","Mario Josue","Perez",
                    "Rodriguez",42);
            verificadorEdad(asistentes);
        } catch(Exception excepcion) {
            excepcion.printStackTrace();
        }
        finally {
            System.out.println("FIN DEL REGISTRO");
        }
    }

    private Personas[] personas;
    public static void verificadorEdad(Personas[] personas) throws Exception{
        for(Personas persona: personas){
            if(persona.getEdad()<=40){
                throw new Exception("La persona no presenta la edad permitida");
            }
        }
    }
}
