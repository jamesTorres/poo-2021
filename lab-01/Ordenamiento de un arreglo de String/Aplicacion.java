package com.principal.Ejercicios.REPASO;

public class Aplicacion {
    public static void main(String[] args) {
        String[] arreglo = {"James","Juan","Jorge","Fernando"};
        for(int i = 0; i < arreglo.length ; i++){
            System.out.print(" "+ arreglo[i]);
        }
        Aplicacion.ordenar(arreglo);
        System.out.println("\n ORDEN DECRECIENTE");

        for(int i = 0; i < arreglo.length ; i++){
            System.out.print(" "+ arreglo[i]);
        }

    }

    public static void ordenar(String[] array){
        for(int i=0; i < array.length-1; i++){
            for(int j = i + 1; j < array.length; j++){
                if(array[i].compareTo(array[j]) > 0){
                    String temporal = array[i];
                    array[i] = array[j];
                    array[j] = temporal;
                }
            }
        }
    }
}
