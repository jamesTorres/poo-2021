package com.principal;

public class Proceso {
    private Venta[] venta;

    public static void main(String[] args) {
        Proceso p = new Proceso();
        p.venta = new Venta[2];
        Producto[] producto1 = new Producto[2];
        Producto pro1 = new Producto();
        pro1.setCodigo(1);
        pro1.setNombre("Toalla");
        pro1.setPrecioUnitario(10.5);
        producto1[0] = pro1;
        Producto pro2 = new Producto();
        pro2.setNombre("Jabon");
        pro2.setPrecioUnitario(7.3);
        pro2.setCodigo(2);
        producto1[1] = pro2;

        p.venta[0] = new Venta();
        p.venta[0].setProducto(producto1);

        Producto[] producto2 = new Producto[2];
        producto2[0] = new Producto();
        producto2[0].setCodigo(1);
        producto2[0].setNombre("Jarra");
        producto2[0].setPrecioUnitario(9.8);
        pro2 = new Producto();
        pro2.setNombre("Agua");
        pro2.setPrecioUnitario(4.2);
        pro2.setCodigo(2);
        producto2[1] = pro2;

        p.venta[1] = new Venta();
        p.venta[1].setProducto(producto2);
        System.out.println("El total de venta:" +p.calcularTotal());

    }
    public Double calcularTotal(){
        Double total = 0D;
        for (int i = 0; i < this.venta.length; i++) {
            for (int j = 0; j < this.venta[i].getProducto().length; j++) {
                total += this.venta[i].getProducto()[j].getPrecioUnitario();
            }
        }
        return total;
    }

}
