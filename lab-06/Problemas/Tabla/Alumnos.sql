CREATE DATABASE Registrolocal;

USE Registrolocal;

CREATE TABLE Registrolocal(
     nombre VARCHAR(100),
     ciclo NUMERIC(3,0)
);

INSERT INTO Registrolocal(nombre,ciclo)
VALUES('Alejandra',5);
INSERT INTO Registrolocal(nombre,ciclo)
VALUES('Esteban',4);
INSERT INTO Registrolocal(nombre,ciclo)
VALUES('Mario',9);
INSERT INTO Registrolocal(nombre,ciclo)
VALUES('Juana',1);
INSERT INTO Registrolocal(nombre,ciclo)
VALUES('Rosa',7);

SELECT *
FROM Registrolocal;
