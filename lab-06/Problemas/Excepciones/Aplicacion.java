package com.principal.Ejercicios.Interfase.Prueba4;

import javax.swing.*;
import java.util.Scanner;

public class Aplicacion {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        int cantidadPersonas;
        cantidadPersonas = Integer.parseInt(JOptionPane.showInputDialog(null,"Cantidad de personas : "));
        int[] Registro = new int[cantidadPersonas];
        System.out.println("Lista de Edades");
        for(int i = 0; i < cantidadPersonas ; i++){
            Registro[i] = Integer.parseInt(JOptionPane.showInputDialog(null,"Edad : "));
            try{
                controlarRestriccion(Registro[i]);
            }catch(ExcepcionLimitedEdad excepcion){
                excepcion.printStackTrace();
                System.out.println("NO PERMITIDO");
            }
            finally {
                System.out.println("Proceso registrado");
            }
        }
    }

    public static void controlarRestriccion(int edad) throws ExcepcionLimitedEdad{
        if(edad < 18){
            throw new ExcepcionLimitedEdad("EDAD NO PERMITIDA");
        }
        else{
            System.out.println("PERMITIDO");
        }
    }
}
