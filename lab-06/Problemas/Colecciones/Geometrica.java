package com.principal.Ejercicios.Colecciones.Prueba5;

import static java.lang.Math.pow;

public class Geometrica extends Serie{

    public Geometrica(Integer terminoEnesimo) {
        super(terminoEnesimo);
    }

    public void generarElementos() {
        for(int i = 0; i < this.terminoEnesimo ; i++){
            if( i == 0){
                this.lista.add(i + 1);
            }
            else{
                this.lista.add((int) pow(2,i));
            }
        }

    }
}
