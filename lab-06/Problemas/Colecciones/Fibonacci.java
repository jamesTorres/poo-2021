package com.principal.Ejercicios.Colecciones.Prueba5;

public class Fibonacci extends Serie{
    public Fibonacci(Integer terminoEnesimo) {
        super(terminoEnesimo);
    }

    public void generarElementos() {
        if(this.terminoEnesimo == 1){
            this.lista.add(1);
        }
        else if(this.terminoEnesimo > 1){
            this.lista.add(1);
            this.lista.add(1);
        }
        for(int i = 2 ; i < this.terminoEnesimo ; i++){
            this.lista.add(this.lista.get(i- 1) + this.lista.get(i - 2));
        }

    }
}
