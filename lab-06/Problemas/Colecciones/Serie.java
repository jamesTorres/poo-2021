package com.principal.Ejercicios.Colecciones.Prueba5;

import java.util.ArrayList;
import java.util.List;

public abstract class Serie {
    protected List<Integer> lista;
    protected Integer terminoEnesimo;

    public Serie(Integer terminoEnesimo) {
        this.lista = new ArrayList<>();
        this.terminoEnesimo = terminoEnesimo;
    }

    public void procesarSerie(){
        generarElementos();
        System.out.println("Suma: "+calcularSumaSerie());
        System.out.println("Promedio: "+calcularPromedioSerie());
    }

    public abstract void generarElementos();

    public Integer calcularSumaSerie(){
        int suma = 0;
        for(Integer numero : lista){
            suma +=numero;
        }
        return suma;
    }

    public double calcularPromedioSerie(){
        Double suma = calcularSumaSerie().doubleValue();
        return suma/terminoEnesimo;
    }

}
