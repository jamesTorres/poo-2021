CREATE DATABASE RegistroPrincipal;

USE RegistroPrincipal;

CREATE TABLE RegistroPrincipal(
     codigo NUMERIC(4,0),
     nombre VARCHAR(100),
     nota NUMERIC(8,2)
);

INSERT INTO RegistroPrincipal(codigo,nombre,nota)
VALUES(33,'Josue',12);
INSERT INTO RegistroPrincipal(codigo,nombre,nota)
VALUES(12,'Juan',15);
INSERT INTO RegistroPrincipal(codigo,nombre,nota)
VALUES(19,'María',17);
INSERT INTO RegistroPrincipal(codigo,nombre,nota)
VALUES(20,'Luis',10);
INSERT INTO RegistroPrincipal(codigo,nombre,nota)
VALUES(21,'Pedro',13);

SELECT *
FROM RegistroPrincipal;

UPDATE RegistroPrincipal
SET nota = 13
WHERE codigo = 20;

UPDATE RegistroPrincipal
SET nota = 15
WHERE codigo = 21;

UPDATE RegistroPrincipal
SET nota = 14
WHERE codigo = 12;


