package com.principal.Ejercicios.Interfase.FactoryMethod;

public class CreadorObjetoB extends Creador{

    public Proceso fabricarObjeto() {
        Proceso proceso = new ObjetoB("Pelota",17.7);
        return proceso;
    }
}
