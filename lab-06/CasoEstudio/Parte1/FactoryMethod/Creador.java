package com.principal.Ejercicios.Interfase.FactoryMethod;

public abstract class Creador {
    public abstract Proceso fabricarObjeto();
}
