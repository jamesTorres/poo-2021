package com.principal.Ejercicios.Interfase.FactoryMethod;

public class ObjetoA implements Proceso{
    private String nombre;
    private Double distancia;

    ObjetoA(String nombre, Double distancia) {
        this.nombre = nombre;
        this.distancia = distancia;
    }

    public double calcularLongitud() {
        return this.distancia;
    }
}
