package com.principal.Ejercicios.Interfase.FactoryMethod;

public class ObjetoB implements Proceso{
    private String nombre;
    private Double distancia;

    ObjetoB(String nombre, Double distancia) {
        this.nombre = nombre;
        this.distancia = distancia;

    }

    public double calcularLongitud() {
        return distancia;
    }
}
