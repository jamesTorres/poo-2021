package com.principal.Ejercicios.Interfase.FactoryMethod;

public class CreadorObjetoA extends Creador {

    public Proceso fabricarObjeto() {
        Proceso proceso = new ObjetoA("Arbol",20.0);
        return proceso;
    }

}
