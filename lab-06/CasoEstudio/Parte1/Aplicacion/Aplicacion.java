package com.principal.Ejercicios.factory.method;

import com.principal.Ejercicios.Interfase.FactoryMethod.*;

public class Aplicacion {
    public static void main(String[] args) {
        double distanciaTotal;
       Creador crearA = new CreadorObjetoA();
       Creador crearB = new CreadorObjetoB();

       Proceso tipoA = crearA.fabricarObjeto();
       Proceso tipoB = crearB.fabricarObjeto();
       distanciaTotal = tipoA.calcularLongitud() +tipoB.calcularLongitud();

        System.out.println("La distancia en metros : "+distanciaTotal+" m");
        System.out.println("La distancia en pies : "+distanciaTotal*3.28+" pies");
    }
}
