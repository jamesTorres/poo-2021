package com.principal.Ejercicios.Interfase.Prueba2;

public interface Figurable {
    public void calcularArea(int lados, int longitud);
    public void calcularPerimetro(int lados, int longitud);
}
