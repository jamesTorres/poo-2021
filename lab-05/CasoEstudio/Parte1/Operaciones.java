package com.principal.Ejercicios.Interfase.Prueba2;

import static java.lang.StrictMath.pow;
import static java.lang.StrictMath.sqrt;

public class Operaciones extends Figura {

    public void areaPoligono(int lados,int longitud) {
        switch(lados){
            case 3:
                resultadoArea = pow(longitud,2)*(sqrt(3)/4);
                break;
            case 4:
                resultadoArea = pow(longitud,2);
                break;
            case 5:
                resultadoArea = (5/4)*pow(longitud,2)*((sqrt(5)+1)/sqrt(10-2*sqrt(5)));
                break;
            case 6:
                resultadoArea = (3/2)*pow(longitud,2)*sqrt(3);
                break;
            case 7:
                resultadoArea = (3.6339) *pow(longitud,2);
        }
        System.out.println("El area es : "+resultadoArea);
    }


    public void perimetroPoligono(int lados,int longitud) {
        switch (lados) {
            case 3:
                resultadoPerimetro = longitud * 3;
                break;
            case 4:
                resultadoPerimetro = longitud* 4;
                break;
            case 5:
                resultadoPerimetro = longitud * 5;
                break;
            case 6:
                resultadoPerimetro = longitud * 6;
                break;
            case 7:
                resultadoPerimetro = longitud * 7;
        }
        System.out.println("El perimetro es : "+resultadoPerimetro);
    }

}
