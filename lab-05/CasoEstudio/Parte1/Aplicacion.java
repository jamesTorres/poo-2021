package com.principal.Ejercicios.Interfase.Prueba2;

import java.util.Scanner;

public class Aplicacion {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        int numeroLados, longitud;
        System.out.print("Cantidad de lados: ");
        numeroLados = entrada.nextInt();
        System.out.print("Longitud: ");
        longitud = entrada.nextInt();
        Operaciones operador = new Operaciones();
        operador.areaPoligono(numeroLados,longitud);
        operador.perimetroPoligono(numeroLados,longitud);
    }
}
