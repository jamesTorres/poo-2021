package com.principal.Ejercicios.Interfase.Prueba2;

public abstract class Figura {
    protected double resultadoArea;
    protected double resultadoPerimetro;
    public abstract void areaPoligono(int lado, int longitud);
    public abstract void perimetroPoligono(int lado, int longitud);
}
