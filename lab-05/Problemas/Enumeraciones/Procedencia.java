package com.principal.Ejercicios.Interfase.Prueba3;

public enum Procedencia {
    TipoA("China","Renminbi"),TipoB("Francia","Euro");
    private String pais;
    private String tipoMoneda;

    Procedencia(String pais, String tipoMoneda) {
        this.pais = pais;
        this.tipoMoneda = tipoMoneda;
    }

    //Se podría utilizar para ver la conversión de monedas peruanas a las de esos países, respecto a
    // las ventas internacionales 
}
