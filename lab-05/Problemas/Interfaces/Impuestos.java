package com.principal.Ejercicios.Interfase.Prueba3;

public class Impuestos implements Registrable {
    private Ventas ventas;
    private double precioReal;
    private double impuestoVenta = 0;

    public double calcularImpuesto() {
        for(Celular celular : this.ventas.getCelulares()){
            precioReal = celular.getCondicionUtilidad() * this.ventas.getPrecio();
            impuestoVenta += precioReal * 0.2 ;
        }
        return impuestoVenta;
    }

    public Ventas getVentas() {
        return ventas;
    }

    public void setVentas(Ventas ventas) {
        this.ventas = ventas;
    }
}
