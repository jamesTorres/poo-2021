package com.principal.Ejercicios.Interfase.Prueba3;

public class Aplicacion {
    public static void main(String[] args) {
        Celular[] celulares = new Celular[3];
        celulares[0] = new Celular("Rojo","Iphone",0.8);
        celulares[1] = new Celular("Negro","Samsung",1);
        celulares[2] = new Celular("Azul","Huawei",0.93);
        Ventas empresaTekno = new Ventas();
        Impuestos empresaGestion = new Impuestos();

        empresaTekno.setCelulares(celulares);
        empresaTekno.setPrecio(1450);       // en el caso que todos se vendan a un mismo precio

        empresaGestion.setVentas(empresaTekno);

        System.out.println("Los impuestos son : "+empresaGestion.calcularImpuesto());
    }
}
