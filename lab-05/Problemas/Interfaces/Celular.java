package com.principal.Ejercicios.Interfase.Prueba3;

public class Celular {
    private String color;
    private String marca;
    private double condicionUtilidad;

    public Celular(String color, String marca, double condicionUtilidad) {
        this.color = color;
        this.marca = marca;
        this.condicionUtilidad = condicionUtilidad;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public double getCondicionUtilidad() {
        return condicionUtilidad;
    }

    public void setCondicionUtilidad(double condicionUtilidad) {
        this.condicionUtilidad = condicionUtilidad;
    }
}
