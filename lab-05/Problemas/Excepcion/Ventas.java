package com.principal.Ejercicios.Interfase.Prueba3;

public class Ventas {
    private Celular[] celulares;
    private int precio;

    public Celular[] getCelulares() {
        return celulares;
    }

    public void setCelulares(Celular[] celulares) {
        this.celulares = celulares;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }
}
