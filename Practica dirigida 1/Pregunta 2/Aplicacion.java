package com.principal.ejercicio6;

import java.util.Scanner;

public class Aplicacion {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        int longitud;
        System.out.print("Numero de lados: ");
        do{
            longitud = entrada.nextInt();
        }while(!(longitud>2 && longitud<7));
        Area variable = new Area();
        System.out.println("Area : "+variable.calcularArea((double)longitud));
        System.out.println("Perimetro : "+Perimetro.calcularPerimetro((double)longitud));
        System.out.println("Diagonales : "+Diagonales.calcularDiagonales(longitud));



    }

}
