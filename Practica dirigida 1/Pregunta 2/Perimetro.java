package com.principal.ejercicio6;


public class Perimetro {
    public static double calcularPerimetro(double longitud) {
        double perimetro = 0;
        if(longitud == 3){
            perimetro = longitud * 3;
        }
        else if(longitud == 4){
            perimetro = longitud * 4;
        }
        else if(longitud == 5){
            perimetro = longitud * 5;
        }
        else if(longitud == 6){
            perimetro = longitud * 6;
        }
        return perimetro;
    }
}
