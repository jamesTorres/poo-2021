package com.principal.ejercicio6;

public class Diagonales {
    public static int calcularDiagonales(int longitud){
        int numeroDiagonales = 0;
        numeroDiagonales = (longitud * (longitud - 3)) / 2;
        return numeroDiagonales;
    }
}
