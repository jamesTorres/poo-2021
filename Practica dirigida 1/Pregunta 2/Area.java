package com.principal.ejercicio6;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class Area {

    public double calcularArea(double longitud){
        double area = 0;
        if(longitud == 3){
            area = pow(longitud,2)*(sqrt(3)/4);
        }
        else if(longitud == 4){
            area = pow(longitud,2);
        }
        else if(longitud == 5){
            area = (5/4)*pow(longitud,2)*((sqrt(5)+1)/sqrt(10-2*sqrt(5)));
        }
        else if(longitud == 6){
            area = (3/2)*pow(longitud,2)*sqrt(3);
        }
        return area;
    }


}
