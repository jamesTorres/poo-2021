package com.principal.ejercicio5;

public class Calzado {
    private String marca;
    private String modelo;
    private String tipo;
    private String genero;
    private String horma;
    private String material;
    private String temporada;
    private String paisOrigen;

    public Calzado(String marca, String modelo, String tipo, String genero, String horma, String material, String temporada, String paisOrigen) {
        this.marca = marca;
        this.modelo = modelo;
        this.tipo = tipo;
        this.genero = genero;
        this.horma = horma;
        this.material = material;
        this.temporada = temporada;
        this.paisOrigen = paisOrigen;
    }

}
