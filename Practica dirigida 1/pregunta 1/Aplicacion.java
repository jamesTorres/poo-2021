package com.principal.ejercicio5;

public class Aplicacion {
    public static void main(String[] args) {
        Ventas[] venta = new Ventas[2];
        // HORNA A = DELGADO Y B = GRUESO
        Calzado calzadosC = new Calzado("Converse", "Antiguo", "Juvenil", "M", "B",
                "Cuero", "Invierno", "Escocia");
        Calzado calzadoN = new Calzado("Nike", "Nuevo", "Infantil", "M", "A",
                "Caucho Sintético", "Verano", "China");

        Cliente cliente1 = new Cliente("James");
        Registro registro1 = new Registro();
        registro1.setCliente(cliente1);
        registro1.setCalzado(calzadosC);
        registro1.setValorUnitario(150);

        venta[0] = new Ventas();
        venta[0].setRegistro(registro1);

        Cliente cliente2 = new Cliente("Luis");
        Registro registro2 = new Registro();
        registro2.setCliente(cliente2);
        registro2.setCalzado(calzadoN);
        registro2.setValorUnitario(99);

        venta[1] = new Ventas();
        venta[1].setRegistro(registro2);

    }
}
